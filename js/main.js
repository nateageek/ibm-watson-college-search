$(document).ready(function() {
	$('#form_title').fadeIn(900, function() {	
	});
	$('#ask_question_form_wrapper').fadeIn(900, function() {	
	});
	$('#ask_question_form').submit(function() {
		console.log("Doing form!");
		$("#waiting_for_watson").fadeIn(400, function() {
			console.log("Should have done the fade in");
		});
		askAboutCollege();
		return false;
	});
	$('#college_name_context').change(function(event) {
		window.location.href = "?college=" + $(this).val();
	});
});

questionList = Array();

function askAboutCollege(){
	var question = $("#question").val();
  var questionText = $("#question").val();
	console.log("QUESTION:"+question);
	$.ajax({
		url: '/askQuestion.php',
		type: 'GET',
		data: {question_value: question, items: 1},
	})
	.done(function(jsonData) {
		var ansObj = JSON.parse(jsonData);
		questionList[ansObj.question.id] = Array();
		questionList[ansObj.question.id]["question_object"] = ansObj;
		questionList[ansObj.question.id]["question_location"] = 0;
 		$("#results").prepend("<div style='display: none;' class='ans_question col-md-10 clearfix' id='"+ansObj.question.id+"'><h4>Question:</h4><p class='ques_text'>"+questionText+"</p><h4>Answer: </h4><p class='ans_text'>"+ansObj.question.evidencelist[0].text+"</p><div class='feed_back_widget'><span id='pre_text'>Was this Answer Useful? </span><a class='feedback_option' id='yes' onClick='sendFeedback(1, \""+ansObj.question.id+"\")'>Yes</a>, <a class='feedback_option' id='kinda' onClick='sendFeedback(9, \""+ansObj.question.id+"\")'>Kinda</a>, <a class='feedback_option' id='no' onClick='sendFeedback(-1, \""+ansObj.question.id+"\")'>No</a></div></div>");
		console.log(ansObj);
		$("#"+ansObj.question.id).slideDown('slow', function() {
			
		});
		$("#waiting_for_watson").fadeOut(400, function() {
			console.log("Should have done the fade out");
		});
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function sendFeedback(score, question_id){
	console.log(questionList[question_id]);
	$.ajax({
		url: '/processFeedback.php',
		type: 'POST',
		data: {question_data: JSON.stringify(questionList[question_id]["question_object"]), feedback_score: score},
	})
	.done(function(resultsFeedback) {
		$("#results").prepend(resultsFeedback);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}
