<?php
	if(empty($_GET['college'])){
		$selected_college = 'default';
	}else{
		$selected_college = $_GET['college'];
		$college_title = ucwords(str_replace("_", " ", $selected_college));
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>NATHAN | A College Connection</title>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    	<script type="text/javascript" src="/js/main.js"></script>
    	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
    	<link rel="stylesheet" type="text/css" href="/css/style.css">
    	<link rel="icon" href="http://murphy.wot.eecs.northwestern.edu/~xzu048/choosecollege/start.png">
	</head>
	<body class="college_<?php print($selected_college); ?>">
		<div id="background_gradient"></div>
		<header id="main_header" class="row">
			<nav class="col-md-12">
				<a href="/"><div id="logo" class="col-md-9">
					<div id="logo_text">
						NATHAN
					</div>
					<div id="sub_logo_text">
						A College Connection
					</div>
				</div></a>
        <ul id="site_nav_links" class="col-md-3">
					<li class="menu_item top_menu">
						<select name="college" id="college_name_context">
							<option value="default">All Colleges</option>
							<option value="northwestern_university">Northwestern University</option>
							<option value="augustana_college">Augustana College</option>
							<option value="loyola_university">Loyola University</option>
							<option value="university_of_illinois_urbana_champaign">University Of Illinois Urbana Champaign</option>
            </select>
					</li>
				</ul>
			</nav>
		</header>
		<section class="container">
			<div id="main_content" class="row">
				<span id="form_title" class="col-md-12">
          <?php if($selected_college == "default"):?>
            NATHAN
            <p style="font-size: 16px;">A College Connection</p>
					<?php else: 
						print($college_title);
						endif;
					?>
				</span>
				<div id="ask_question_form_wrapper" class="col-md-12">
					<form id="ask_question_form">
						<input id="college_contex" type="hidden" value="<?php print($selected_college);?>"/>
						<input id="question" class="col-md-7 col-sm-7 col-xs-12" placeholder="e.g. What is the dining hall food like?"/>
            <input id="submit_question" type="submit" class="col-sm-2 col-xs-12" value="Submit"/>
<!--             <div id="remove_user_reviews_wrapper" class="col-md-5 col-sm-5 col-xs-12">
              <input id="remove_user_reviews" type="checkbox"><label for="remove_user_reviews">Remove User Reviews</label>
            </div> -->
					</form>
				</div>
			</div>
		</section>
		<section id="results_wrapper" class="container">
    	<div id="waiting_for_watson" class="col-md-12">
    		<div id="ball_1" class="ball"></div>
    		<div id="ball_2" class="ball"></div>
    		<div id="ball_3" class="ball"></div>
			</div>
			<section id="results" class="container">
			</section>
		</section>
	</body>
</html>
